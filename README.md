# README #

### What's the point of this?

It's the WIP code for the Order of Precedence for the Kingdom of Ansteorra, after the new database format is finalized.  It's probably of no interest to anybody but it makes my life simpler to not use a private repo, plus what the heck, maybe someday we'll get our FOSS really on and make something awesome of it.  Not that I don't think it's awesome.

### Yeah, okay.  What's it use?

First thing it uses is composer to install all the other things it uses.  Well, no, first thing it uses is PHP.  It's tested under PHP 7.1 and I don't overly care if it works with anything before 7.  Always moving forward.  Anyway, like I said, composer should install everything else, which is to say the Slim framework, Illuminate/Database and Illuminate/Pagination from the good folks at Laravel, and the Twig view extension for Slim.  Twig is awesome.  Actually, it's all good stuff which I have so far criminally underused.  You will notice the lack of any ORM anything, for example.  Twig, though, is the heart of making this thing not awful to deal with.

Oh yeah, and Ink for the CSS.  I'm pretty fickle about Ink and Cardinal and Bootstrap and half a dozen others, but this one's using Ink.

### No Javascript?

Not currently.  The previous version of this thing was entirely written in Javascript on the front end (Coffeescript and React, for the record) and then again Illuminate on the backend alongside with FastRoute. It worked pretty okay but it all felt really brittle.  This time I want to start with the version that will work solidly and then see what bells and whistles I might add to make it do less page loading and some more dynamic reaction.  I'm thinking probably Livescript and Vue.js but I haven't really started down that road yet.

I was really tempted to rewrite it all in Elm but as I say, I wanted to make it all work from the backend first, and Elm doesn't really lend itself to integration with that.  Vue.js, in theory, does, much like Angular v1 used to.  In theory.  We'll see how it goes, whenever I get to it.

### If someone **did** want to help out, what about test data?

Uh, yeah.  I have a test database but I'm not giving you the credentials.  On the other hand, there's not really any secrets in the OP database, but that may change.  So... I guess if the time comes I'll figure something out, maybe an excerpt of what we've currently got?  I dunno, worst case I'll certainly share the schema so someone can roll their own test data.  And then share it with us.

Does Bitbucket have a way for people to contact me?  No idea.  Maybe I'll move this thing to Github in the long run if it's warranted.

Reis