<?php
// vim:set ai sw=4 ts=4 et:
require_once("vendor/autoload.php");
require_once("db.php");

error_reporting( error_reporting() & ~E_NOTICE );

$app = new \Slim\App(OPConfig);
$container = $app->getContainer();
$container['db'] = function($container) {
    $db = new \Illuminate\Database\Capsule\Manager;
    $db->addConnection($container['settings']['db']);
    $db->setAsGlobal();
    $db->bootEloquent();
    return $db;
};
$container['view'] = function($container) {
    $view = new \Slim\Views\Twig('templates');
    $view->addExtension(new \Slim\Views\TwigExtension($container['router'],'/'));
    return $view;
};

$app->get('/names', function($req,$resp) {
    return $this->view->render($resp,'name_search.html',['current'=>'name']);
})->setName('name_search');

$app->get('/search/name', function($req,$resp) {
    if (!isset($req->getQueryParams()['clear'])) {
        ['search'=>$search,
         'startName'=>$startName,
         'matchCase'=>$matchCase,
         'searchOther'=>$searchOther] = $req->getQueryParams();
    };
    if (!isset($search)) return $resp->withHeader('Location','/names');
    $op = $matchCase ? 'like binary' : 'like';
    $pat = $startName ? $search.'%' : '%'.$search.'%';
    $people = $this->db
                   ->table('people')
                   ->where('name',$op,$pat)
                   ->when($searchOther,function($q) use($op,$pat) {
                       return $q->orWhere('other_names',$op,$pat);
                     })
                   ->paginate(15,['*'],'page',$req->getQueryParam('page'));
    return $this->view->render($resp,'name_search.html',
        ['current'=>'name', 'req'=>$req, 'results'=>$people]);
})->setName('search_by_name');

$app->get('/name/id/{id}', function($req,$resp,$args) {
    $sort = $req->getQueryParam('sort');
    $person = $this->db
                   ->table('people')
                   ->leftJoin('achievements','people.id','=','achievements.person_id')
                   ->select('people.*','achievements.id as achievement')
                   ->where('people.id',$args['id'])->first();
    $given = $this->db
                  ->table('awardsgiven')
                  ->where('person_id','=',$args['id'])
                  ->join('awards','awardsgiven.award_id','=','awards.id')
                  ->orderBy($sort?$sort:'dategiven',$sort=='precedence'?'desc':'asc')
                  ->paginate(15,['*'],'page',$req->getQueryParam('page'));
    return $this->view->render($resp,'name_search.html',
        ['current'=>'name',
         'req'=>$req,
         'person'=>$person,
         'given'=>$given,
         'id'=>$args['id']]);
})->setName('name_by_id');

$app->get('/name/csv/{id}', function($req,$resp,$args) {
    $given = $this->db->setFetchMode(PDO::FETCH_NUM)
                  ->table('awardsgiven')
                  ->where('person_id','=',$args['id'])
                  ->join('awards','awardsgiven.award_id','=','awards.id')
                  ->join('events','events.id','=','awardsgiven.event_id')
                  ->leftJoin('crowns','crowns.id','=','awardsgiven.crown_id')
                  ->select(['awards.name','events.name','dategiven','notes','crowns.names','imported','resigndate'])
                  ->orderBy('dategiven','asc')
                  ->get();
    $fp = fopen("php://temp","r+");
    fputs($fp,'"Award","Event","Date","Notes","Crown","Imported","Resign Date"'."\n");
    foreach ($given as $item) {
        fputcsv($fp,$item);
    }
    rewind($fp);
    $resp->getBody()->write(stream_get_contents($fp));
    return $resp->withHeader('Content-Type','text/csv')
                ->withHeader('Content-Disposition',
                    'attachment; filename="awardsforperson.csv"');
});

$app->get('/awards', function($req,$resp) {
    return $this->view->render($resp,'award_search.html',['current'=>'award']);
})->setName('award_search');

$app->get('/search/award', function($req,$resp) {
    if (!isset($req->getQueryParams()['clear'])) {
        ['search'=>$search,
         'startName'=>$startName,
         'matchCase'=>$matchCase] = $req->getQueryParams();
    };
    if (!isset($search)) return $resp->withHeader('Location','/awards');
    $op = $matchCase ? 'like binary' : 'like';
    $pat = $startName ? $search.'%' : '%'.$search.'%';
    $awards = $this->db
                   ->table('awards')
                   ->where('name',$op,$pat)
                   ->paginate(15,['*'],'page',$req->getQueryParam('page'));
    return $this->view->render($resp,'award_search.html',
        ['current'=>'award', 'req'=>$req, 'results'=>$awards]);
})->setName('search_by_award');

$app->get('/award/id/{id}', function($req,$resp,$args) {
    $sort = $req->getQueryParam('sort');
    $award = $this->db
                   ->table('awards')
                   ->find($args['id']);
    $given = $this->db
                  ->table('awardsgiven')
                  ->where('award_id','=',$args['id'])
                  ->join('people','awardsgiven.person_id','=','people.id')
                  ->orderBy($sort?$sort:'dategiven','asc')
                  ->paginate(15,['*'],'page',$req->getQueryParam('page'));
    return $this->view->render($resp,'award_search.html',
        ['current'=>'award',
         'req'=>$req,
         'award'=>$award,
         'given'=>$given,
         'id'=>$args['id']]);
})->setName('award_by_id');

$app->get('/award/csv/{id}', function($req,$resp,$args) {
    $given = $this->db->setFetchMode(PDO::FETCH_NUM)
                  ->table('awardsgiven')
                  ->where('award_id','=',$args['id'])
                  ->join('people','people.id','=','awardsgiven.person_id')
                  ->join('events','events.id','=','awardsgiven.event_id')
                  ->leftJoin('crowns','crowns.id','=','awardsgiven.crown_id')
                  ->select(['people.name','events.name','dategiven','notes','crowns.names','imported','resigndate'])
                  ->orderBy('dategiven','asc')
                  ->get();
    $fp = fopen("php://temp","r+");
    fputs($fp,'"Person","Event","Date","Notes","Crown","Imported","Resign Date"'."\n");
    foreach ($given as $item) {
        fputcsv($fp,$item);
    }
    rewind($fp);
    $resp->getBody()->write(stream_get_contents($fp));
    return $resp->withHeader('Content-Type','text/csv')
                ->withHeader('Content-Disposition',
                    'attachment; filename="peopleforaward.csv"');
});

$app->get('/events', function($req,$resp) {
    return $this->view->render($resp,'event_search.html',['current'=>'event']);
})->setName('event_search');

$app->get('/search/event', function($req,$resp) {
    if (!isset($req->getQueryParams()['clear'])) {
        ['search'=>$search,
         'startName'=>$startName,
         'matchCase'=>$matchCase] = $req->getQueryParams();
    };
    if (!isset($search)) return $resp->withHeader('Location','/events');
    $op = $matchCase ? 'like binary' : 'like';
    $pat = $startName ? $search.'%' : '%'.$search.'%';
    $events = $this->db
                   ->table('events')
                   ->where('name',$op,$pat)
                   ->paginate(15,['*'],'page',$req->getQueryParam('page'));
    return $this->view->render($resp,'event_search.html',
        ['current'=>'event', 'req'=>$req, 'results'=>$events]);
})->setName('search_by_event');

$app->get('/event/id/{id}', function($req,$resp,$args) {
    $sort = $req->getQueryParam('sort');
    $event = $this->db
                   ->table('events')
                   ->find($args['id']);
    $given = $this->db
                  ->table('awardsgiven')
                  ->where('event_id','=',$args['id'])
                  ->join('awards','awardsgiven.award_id','=','awards.id')
                  ->join('people','awardsgiven.person_id','=','people.id')
                  ->select('*','awards.name as awardname','people.name as personname','awards.id as awardid','people.id as personid')
                  ->orderBy($sort?$sort:'personname',$sort=='precedence'?'desc':'asc')
                  ->paginate(15,['*'],'page',$req->getQueryParam('page'));
    return $this->view->render($resp,'event_search.html',
        ['current'=>'event',
         'req'=>$req,
         'event'=>$event,
         'given'=>$given,
         'id'=>$args['id']]);
})->setName('event_by_id');

$app->get('/event/csv/{id}', function($req,$resp,$args) {
    $given = $this->db->setFetchMode(PDO::FETCH_NUM)
                  ->table('awardsgiven')
                  ->where('event_id','=',$args['id'])
                  ->join('people','people.id','=','awardsgiven.person_id')
                  ->join('awards','awards.id','=','awardsgiven.award_id')
                  ->leftJoin('crowns','crowns.id','=','awardsgiven.crown_id')
                  ->select(['people.name','awards.name','dategiven','notes','crowns.names','imported','resigndate'])
                  ->orderBy('dategiven','asc')
                  ->get();
    $fp = fopen("php://temp","r+");
    fputs($fp,'"Person","Award","Date","Notes","Crown","Imported","Resign Date"'."\n");
    foreach ($given as $item) {
        fputcsv($fp,$item);
    }
    rewind($fp);
    $resp->getBody()->write(stream_get_contents($fp));
    return $resp->withHeader('Content-Type','text/csv')
                ->withHeader('Content-Disposition',
                    'attachment; filename="awardsforevent.csv"');
});

$app->run();
